<?php

namespace WDate;

/**
 * Class WDate
 * @package WDate
 */
class WDate
{
    const TYPE_FULL_DATETIME                = 1;
    const TYPE_DATETIME_WITHOUT_SECONDS     = 2;
    const TYPE_DATE_WITH_HOUR               = 4;
    const TYPE_DATE                         = 8;
    const TYPE_YEAR_MONTH                   = 16;
    const TYPE_YEAR                         = 32;
    const TYPE_HOUR                         = 64;
    const TYPE_HOUR_MINUTES                 = 128;
    const TYPE_TIME                         = 256;

    /**
     * Шаблоны поиска возможных комбинаций даты и времени
     *
     * @var array
     */
    protected $_findPatterns   = [
        self::TYPE_FULL_DATETIME                => '~^(?<hour>\d{2}):(?<minutes>\d{2}):(?<seconds>\d{2}) (?<day>\d{2})\.(?<month>\d{2})\.(?<year>\d{4})$~',
        self::TYPE_DATETIME_WITHOUT_SECONDS     => '~^(?<hour>\d{2}):(?<minutes>\d{2}) (?<day>\d{2})\.(?<month>\d{2})\.(?<year>\d{4})$~',
        self::TYPE_DATE_WITH_HOUR               => '~^(?<hour>\d{2}):? (?<day>\d{2})\.(?<month>\d{2})\.(?<year>\d{4})$~',
        self::TYPE_DATE                         => '~^(?<day>\d{2})\.(?<month>\d{2})\.(?<year>\d{4})$~',
        self::TYPE_YEAR_MONTH                   => '~^(?<month>\d{2})\.(?<year>\d{4})$~',
        self::TYPE_YEAR                         => '~^(?<year>\d{4})$~',
        self::TYPE_HOUR                         => '~^(?<hour>\d{2})$~',
        self::TYPE_HOUR_MINUTES                 => '~^(?<hour>\d{2}):(?<minutes>\d{2})$~',
        self::TYPE_TIME                         => '~^(?<hour>\d{2}):(?<minutes>\d{2}):(?<seconds>\d{2})$~'
    ];

    /**
     * Группы сравнения, форматы одной группы можно сравнивать между собой
     *
     * @var array
     */
    protected $_compareGroups   = [
        self::TYPE_FULL_DATETIME | self::TYPE_DATETIME_WITHOUT_SECONDS | self::TYPE_DATE_WITH_HOUR | self::TYPE_DATE | self::TYPE_YEAR_MONTH | self::TYPE_YEAR,
        self::TYPE_HOUR | self::TYPE_HOUR_MINUTES | self::TYPE_TIME
    ];

    /**
     * Формат даты и времени для создания объекта \DateTime
     *
     * @var array
     */
    protected $_formats = [
        self::TYPE_FULL_DATETIME                => 'hour:minutes:seconds day.month.year',
        self::TYPE_DATETIME_WITHOUT_SECONDS     => 'hour:minutes:00 day.month.year',
        self::TYPE_DATE_WITH_HOUR               => 'hour:00:00 day.month.year',
        self::TYPE_DATE                         => '00:00:00 day.month.year',
        self::TYPE_YEAR_MONTH                   => '00:00:00 01.month.year',
        self::TYPE_YEAR                         => '00:00:00 01.01.year',
        self::TYPE_HOUR                         => 'hour:00:00 d.m.Y',
        self::TYPE_HOUR_MINUTES                 => 'hour:minutes:00 d.m.Y',
        self::TYPE_TIME                         => 'hour:minutes:seconds d.m.Y'
    ];

    /**
     * Исходная дата
     *
     * @var string
     */
    protected $_inputDate;

    /**
     * Определенный тип даты
     *
     * @var int
     */
    protected $_dateType;

    /**
     * Результат поиска по шаблону
     *
     * @var array
     */
    protected $_parseResult = [];

    /**
     * Объект даты
     *
     * @var \DateTime
     */
    protected $_objectDate;

    /**
     * Группа для сравнения
     *
     * @var int
     */
    protected $_compareGroup;

    /**
     * WDate constructor.
     *
     * @param $date
     * @throws WDateException
     */
    public function __construct($date)
    {
        $this->_inputDate = $date;
        try {
            $this->_parseDate();
        } catch (WDateException $e) {
            throw $e;
        }
    }

    /**
     * Получение объекта даты для сравнения
     *
     * @return \DateTime
     * @throws WDateException
     */
    public function getDate()
    {
        try {
            return new \DateTime($this->_prepareFormatDate());
        } catch (\Exception $e) {
            throw new WDateException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * Получение исходной даты
     *
     * @return string
     */
    public function getInputDate()
    {
        return $this->_inputDate;
    }

    public function getCompareGroup()
    {
        return $this->_compareGroup;
    }

    public function getType()
    {
        return $this->_dateType;
    }

    public function getYear()
    {
        return $this->_parseResult['year'] ? $this->_parseResult['year'] : false;
    }

    public function getMonth()
    {
        return $this->_parseResult['month'] ? $this->_parseResult['month'] : false;
    }

    public function getDay()
    {
        return $this->_parseResult['day'] ? $this->_parseResult['day'] : false;
    }

    public function getHour()
    {
        return $this->_parseResult['hour'] ? $this->_parseResult['hour'] : false;
    }

    public function getMinutes()
    {
        return $this->_parseResult['minutes'] ? $this->_parseResult['minutes'] : false;
    }

    public function getSeconds()
    {
        return $this->_parseResult['seconds'] ? $this->_parseResult['seconds'] : false;
    }

    /**
     * @param WDate $comparable
     * @return int
     * @throws WDateException
     */
    public function compare(WDate $comparable)
    {
        // Если типы совпадают, или форматы из одной группы, можно сравнивать
        if ($this->getType() === $comparable->getType() || $this->getCompareGroup() === $comparable->getCompareGroup()) {
            try {
                if ($this->getDate() == $comparable->getDate()) {
                    if ($this->getType() === $comparable->getType()) {
                        return 0;
                    } elseif ($this->getType() > $comparable->getType()) {
                        return -1;
                    }
                    return 1;
                } elseif ($this->getDate() > $comparable->getDate()) {
                    return 1;
                }
                return -1;
            } catch (WDateException $e) {
                throw $e;
            }
        }
        if ($this->getCompareGroup() < $comparable->getCompareGroup()) {
            return 1;
        }
        return -1;
    }

    /**
     * @throws WDateException
     */
    protected function _parseDate()
    {
        foreach ($this->_findPatterns as $type => $pattern) {
            if ( ! preg_match($pattern, $this->_inputDate, $result)) continue;
            $this->_dateType    = $type;
            $this->_parseResult = array_intersect_key($result, array_flip(['year','month','day','hour','minutes','seconds']));
        }
        if ( ! $this->_dateType) {
            throw new WDateException('Pattern not found');
        }
        $this->_identifyCompareGroup();
    }

    /**
     * @return string
     */
    protected function _prepareFormatDate()
    {
        return date(strtr($this->_formats[$this->_dateType], $this->_parseResult));
    }

    protected function _identifyCompareGroup()
    {
        foreach ($this->_compareGroups as $group => $types) {
            if ($types & $this->_dateType) {
                $this->_compareGroup    = $group;
                break;
            }
        }
    }
}