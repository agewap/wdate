WDate
=====

Установка
---------
```
git clone https://bitbucket.org/agewap/wdate
composer install
```

Использование
-------------
```php
require 'vendor/autoload.php';

try {
    $date   = new \WDate\WDate('06.12.2017');
    $date2  = new \WDate\WDate('22:26');
} catch (\WDate\WDateException $e) {
	// Обработка исключения
}
$date->compare($date2); // Возвращает 1 если $date больше $date2, -1 если $date меньше $date2 и 0 если равны
```