<?php

require dirname(__DIR__) . '/vendor/autoload.php';

class WDateExceptionTest extends PHPUnit\Framework\TestCase
{
    /**
     * @expectedException \WDate\WDateException
     */
    public function testException()
    {
        new \WDate\WDate('2017-10-06');
    }
}