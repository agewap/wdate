<?php

require dirname(__DIR__) . '/vendor/autoload.php';

class WDateSortTest extends PHPUnit\Framework\TestCase
{
    public function testSort()
    {
        $date1  = new \WDate\WDate('10.2019');
        $date2  = new \WDate\WDate('22:35');
        $date3  = new \WDate\WDate('23.05.2016');
        $date4  = new \WDate\WDate('14:02 11.10.2015');

        $unsorted  = [
            $date1,
            $date2,
            $date3,
            $date4
        ];

        $sorted = [
            $date1,
            $date3,
            $date4,
            $date2
        ];

        usort($unsorted, function($a, $b)
        {
            /**
             * @var $a \WDate\WDate
             * @var $b \WDate\WDate
             */
            return $b->compare($a);
        });

        $this->assertEquals($sorted[0]->getInputdate(), $unsorted[0]->getInputdate());
        $this->assertEquals($sorted[1]->getInputdate(), $unsorted[1]->getInputdate());
        $this->assertEquals($sorted[2]->getInputdate(), $unsorted[2]->getInputdate());
        $this->assertEquals($sorted[3]->getInputdate(), $unsorted[3]->getInputdate());
    }
}