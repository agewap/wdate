<?php

require dirname(__DIR__) . '/vendor/autoload.php';

class WDateCompareTest extends PHPUnit\Framework\TestCase
{
    /**
     * @param $exceptedResult
     * @param $compareResult
     * @dataProvider providerCompare
     */
    public function testCompare($exceptedResult, $compareResult)
    {
        $this->assertEquals($exceptedResult, $compareResult);
    }

    public function providerCompare()
    {
        $date1  = new \WDate\WDate('16:45:38 10.04.2017');
        $date2  = new \WDate\WDate('14:20 09.06.2016');
        $date3  = new \WDate\WDate('14: 23.03.2010');
        $date4  = new \WDate\WDate('19 22.07.2007');
        $date5  = new \WDate\WDate('30.05.2010');
        $date6  = new \WDate\WDate('10.1999');
        $date7  = new \WDate\WDate('2001');
        $date8  = new \WDate\WDate('22:15:04');
        $date9  = new \WDate\WDate('23:10');
        $date10 = new \WDate\WDate('18');
        $date11 = new \WDate\WDate('11.2002');
        $date12 = new \WDate\WDate('01.11.2002');
        $date13 = new \WDate\WDate('2017');
        $date14 = new \WDate\WDate('10.2020');
        $date15 = new \WDate\WDate('30.05.2010');
        $date16 = new \WDate\WDate('23:10');

        try {
            return [
                [1, $date1->compare($date10)],
                [-1, $date2->compare($date1)],
                [1, $date7->compare($date6)],
                [-1, $date5->compare($date2)],
                [1, $date9->compare($date8)],
                [1, $date3->compare($date4)],
                [1, $date11->compare($date6)],
                [-1, $date7->compare($date11)],
                [1, $date12->compare($date11)],
                [-1, $date4->compare($date5)],
                [-1, $date13->compare($date1)],
                [1, $date14->compare($date1)],
                [0, $date15->compare($date5)],
                [0, $date16->compare($date9)],
                [-1, $date10->compare($date9)]
            ];
        } catch (\WDate\WDateException $e) {
            $this->fail($e->getMessage());
        }
    }
}